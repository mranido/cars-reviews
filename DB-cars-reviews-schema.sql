CREATE DATABASE IF NOT EXISTS cars_reviews;
Use cars_reviews;

CREATE TABLE IF NOT EXISTS cars(
	id INT NOT NULL auto_increment,
    marca VARCHAR (100) not null,
    modelo VARCHAR(255) not null,
    anho int not null,
    motor ENUM('Diésel', 'Gasolina', 'Híbrido', 'Eléctrico') DEFAULT 'Gasolina',
    cv int null,
	createdAt DATETIME Not null,
    updatedAt datetime null,
    deletedAt datetime null,
    primary key(id)
);