"use strict";

const { findAllCars } = require("../../repositories/cars-repository");
//const carsRepository = require("../../repositories/cars-repository");
async function getCars(req, res) {
  try {
    //const cars = await carsRepository.findAll();
    const cars = await findAllCars();
    res.status(200);
    res.send(cars);
  } catch (err) {
    console.log(err.message);
  }
}

module.exports = { getCars };
