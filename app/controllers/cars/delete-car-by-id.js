"use strict";

const Joi = require("joi");
const schema = Joi.number().integer().positive().required();
const {
  deleteCar,
  findCarById,
} = require("../../repositories/cars-repository");

async function deleteCarById(req, res) {
  try {
    const { idCar } = req.params;
    await schema.validateAsync(idCar);
    console.log("DELETE CAR", req.body);

    const car = await findCarById(idCar);
    if (!car) {
      throw new Error("Coche no existe");
    }
    await deleteCar(idCar);
    res.status(204);
    res.end();
  } catch (err) {
    console.log(err.message);
  }
}

module.exports = { deleteCarById };
