"use strict";

const cars = [
  {
    id: 1,
    marca: "Seat",
    modelo: "Ibiza",
    anho: 2019,
    motor: "Diesel",
    cv: 125,
  },
  {
    id: 2,
    marca: "Opel",
    modelo: "Corsa",
    anho: 2014,
    motor: "Diesel",
    cv: 80,
  },
  {
    id: 3,
    marca: "Audi",
    modelo: "A3",
    anho: 2017,
    motor: "Gasolina",
    cv: 150,
  },
];

async function findAllCars() {
  //await SELECT * FROM cars;
  return cars;
}

async function findCarById(idCar) {
  //await select * from cars where id = +idCar;
  return cars.find((car) => car.id === +idCar);
}

async function addCar(car) {
  //llamada a la base de datos
  //const id = Math.max(...cars.map((car) => car.id)) + 1;
  const id = cars.reduce((acc, car) => (car.id > acc ? car.id : acc), 0) + 1;
  console.log("nuevoID", id);
  const newCar = {
    id: id,
    ...car,
  };
  cars.push(newCar);
  return true;
}

async function deleteCar(idcar) {
  const car = await findCarById(idcar);
  console.log(car);
  const index = cars.indexOf(car);
  console.log(index);

  console.log("Borrando ...");
  return cars.splice(index, 1);
}

module.exports = { addCar, deleteCar, findAllCars, findCarById };
