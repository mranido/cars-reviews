"use strict";

const express = require("express");
const router = express.Router();
const { getCars } = require("../controllers/cars/get-cars.js");
const { getCarById } = require("../controllers/cars/get-car-by-id.js");
const { createCar } = require("../controllers/cars/create-car.js");
const { deleteCarById } = require("../controllers/cars/delete-car-by-id.js");

router.route("/").get(getCars);
router.route("/").post(createCar);
router.route("/:idCar").get(getCarById);
router.route("/:idCar").delete(deleteCarById);
module.exports = router;
