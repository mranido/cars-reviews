"use strict";

require("dotenv").config();
const express = require("express");
const app = express();
app.use(express.json());

const carsRouter = require("./app/routes/cars-routes.js");

const port = process.env.SERVER_PORT || 3000;

// api/v1/cars/:id
app.use("/api/v1/cars/", carsRouter);

app.listen(port, () => console.log(`Escuchando puerto ${port}`));
